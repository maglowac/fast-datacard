imax    *     number of categories
jmax    *     number of samples minus one
kmax    *     number of nuisance parameters
-------------------------------------------------------------------------------
shapes * * eq0b_ge2a_900_inf_shapes.root $CHANNEL/$PROCESS $CHANNEL/$PROCESS_$SYSTEMATIC

bin	 Signal	DoubleMu	SingleMu	
observation	134	33	39	
-------------------------------------------------------------------------------
bin	Signal	Signal	Signal	Signal	DoubleMu	DoubleMu	SingleMu	SingleMu	

process	 VBF	Zinv	Ttw	Qcd	VBF	Ewk	VBF	Ewk	
process	 0	1	2	3	0	1	0	1	
rate	 1e-12	41.0142498016	45.5570440292	51.4110374451	1e-12	26.2619781494	1e-12	21.0961380005	
-------------------------------------------------------------------------------
ElectronSFWeight shape	1	-	-	-	1	-	1	-	
bosonPtEwkWeight shape	1	1	1	1	1	1	1	1	
bosonPtQcdWeight shape	1	1	1	1	1	1	1	1	
bosonPtWeight shape	1	1	1	1	1	1	1	1	
bsfCFbWeight shape	1	-	-	-	1	-	1	-	
bsfCFcWeight shape	1	-	-	-	1	-	1	-	
bsfCFlWeight shape	1	-	-	-	1	-	1	-	
bsfLightWeight shape	1	1	1	1	1	1	1	1	
bsfWeight shape	1	1	1	1	1	1	1	1	
jecWeight shape	-	1	1	1	-	1	-	1	
lumiSyst lnN	1.026	1.06	-	-	1.026	1.06	1.026	1.06	
muonSfWeight shape	1	1	1	1	1	1	1	1	
muonTrackWeight shape	1	-	-	-	1	-	1	-	
nIsrWeight shape	1	1	1	1	1	1	1	1	
photonSFWeight shape	1	1	1	1	1	1	1	1	
photonTriggerWeight shape	1	1	1	1	1	1	1	1	
puWeight shape	1	1	1	1	1	1	1	1	
triggerWeight shape	1	1	1	1	1	1	1	1	
xsWeightTt shape	1	1	1	1	1	1	1	1	
xsWeightW shape	1	1	1	1	1	1	1	1	
