#!/usr/bin/env python
from __future__ import print_function
import glob
from array import array
import pandas as pd
# from . import io
from . import root_wrapper as rootw
import alphatwirl
import os
# import collections
# import math
from jinja2 import Template
# import functools


class Analysis:

    analysis_name = ""
    version = ""
    dataset = ""
    luminosity = 0.
    path_to_dfs = ""
    include_category_name = True
    autoMCStats_threshold = 0.

    regions = []

    categories_df = ""
    categories_pattern = []
    categories = {}

    data_names_df = ""
    data_names_dc = ""

    signals = []
    backgrounds = []
    systematics = []
    appendix = []

    def __init__(self, config_file):
        with open(config_file, "r") as infile:
            import yaml
            cfg = yaml.load(infile)
            self.analysis_name = str(cfg["general"]["analysis_name"])
            self.version = str(cfg["general"]["version"])
            self.dataset = str(cfg["general"]["dataset"])
            self.luminosity = float(cfg["general"]["luminosity"])
            self.path_to_dfs = str(cfg["general"]["path_to_dfs"])
            self.include_category_name = bool(cfg["general"]["include_category_name"])
            self.autoMCStats_threshold = 0.
            
            if "autoMCStats_threshold" in cfg["general"]:
                self.autoMCStats_threshold = float(cfg["general"]["autoMCStats_threshold"])
            else:
                self.autoMCStats_threshold = 0.

            self.regions = cfg["regions"]["regions"]

            self.data_names_df = str(cfg["data"]["data_names_df"])
            self.data_names_dc = str(cfg["data"]["data_names_dc"])

            self.signals = cfg["signals"]
            self.backgrounds = cfg["backgrounds"]
            self.systematics = cfg["systematics"]
            self.appendix = cfg["appendix"]


def getCategorization(myAnalysis, query=None):

    name_df = ""
    for signal in myAnalysis.signals:
        name_df = os.path.join(myAnalysis.path_to_dfs, signal["name"] + ".csv")
        continue

    reader = pd.read_csv(name_df, sep=None, iterator=True, engine='python')
    inferred_sep = reader._engine.data.dialect.delimiter
    df = pd.read_csv(name_df, delim_whitespace=True)
    if inferred_sep == ",":
        df = pd.read_csv(name_df)

    if query:
        df=df.query(query, engine='python')

    # df = io.custom_pd_read_table(
    #    df_name,
    #    dtype={
    #        'variable_low': str,
    #        'variable_high': str})
    # df = pd.read_csv(df_name)

    groupByList = ["category", "region", "variable"]
    groups = df.groupby(groupByList)

    categories = {}

    for name, group in groups:
        sorted_bins = sorted(
            group["variable_low"].tolist() + group["variable_high"].tolist())
        sorted_bins = list(set(sorted_bins))
        if name[groupByList.index("category")] not in categories:
            categories[name[groupByList.index("category")]] = {}
        list1 = sorted(list(group["variable_low"].tolist()))
        list2 = sorted(list(group["variable_high"].tolist()))
        binning = list1 + list2
        binning_tmp = [float(i) for i in binning if i != "inf"]
        binning_tmp = sorted(list(set(binning_tmp)))
        binning_tmp = [str(i) for i in binning_tmp]
        binning_tmp2 = ["inf" for i in binning if str(i) == "inf"]
        binning = sorted(list(set(binning_tmp + binning_tmp2)))

        categories[name[groupByList.index("category")]][name[groupByList.index("region")]] = [
            name[groupByList.index("variable")], binning]

    return categories


def createHisto(name, binning):

    histo = rootw.TH1F(name, name, len(binning) - 1, array('f', binning))

    return histo


def createDictionary(myAnalysis, categories, type_processes, query):

    if type_processes == "signals":
        list_processes = myAnalysis.signals
    elif type_processes == "backgrounds":
        list_processes = myAnalysis.backgrounds
    elif type_processes == "data":
        list_processes = ["data"]

    histDict = {}
    name_df = os.path.join(myAnalysis.path_to_dfs, myAnalysis.data_names_df + ".csv")
    reader = pd.read_csv(name_df, sep=None, iterator=True, engine='python')
    inferred_sep = reader._engine.data.dialect.delimiter
    main_df = pd.read_csv(name_df, delim_whitespace=True)
    if inferred_sep == ",":
            main_df = pd.read_csv(name_df)
    if query:
        main_df = main_df.query(query, engine='python')

    for category in categories:

        histDict[category] = {}

        df = main_df[main_df["category"] == category]
        regions_to_consider = df["region"].unique()

        for region in regions_to_consider:

            histDict[category][region] = {}

            for process in list_processes:

                if type_processes == "backgrounds":

                    allowed_region = []
                    for i, value in enumerate(myAnalysis.backgrounds):
                        if myAnalysis.backgrounds[i]["name"] != process["name"]:
                            continue
                        allowed_region = myAnalysis.backgrounds[i]["regions"]

                    if region not in allowed_region:
                        continue

                if type_processes == "signals":

                    allowed_region = []
                    for i, value in enumerate(myAnalysis.signals):
                        if myAnalysis.signals[i]["name"] != process["name"]:
                            continue
                        allowed_region = myAnalysis.signals[i]["regions"]

                    if region not in allowed_region:
                        continue

                if type_processes != "data":
                    histDict[category][region][process["name"]] = {}
                else:
                    histDict[category][region]["data"] = {}

    return histDict


def fillDictionary(myAnalysis, histDictionary, type_processes, categories, query=None):
    """ Fills the histograms within the dictionary with yield values from the dataframes.
    Histograms are stored in the output root datacards and their integrals used in the output txt datacards
        :param myAnalysis instance of Analysis, holds configuration
        :param histDictionary initialized with histograms that need to be filled
        :param type_processes is "signals", "backgrounds", or "data" and determines the dictionary to be filled
    """

    # histDictionary is a nested dict of TH1: histDictionary[category][region][process][systematic] = TH1
    if type_processes == "signals":
        list_processes = myAnalysis.signals
    elif type_processes == "backgrounds":
        list_processes = myAnalysis.backgrounds
    elif type_processes == "data":
        list_processes = ["data"]

    # loop on processes
    for process in list_processes:

        # opening corresponding dataframe
        name_df = ""
        if type_processes != "data":
            name_df = os.path.join(myAnalysis.path_to_dfs, process["name"] + ".csv")
        else:
            name_df = os.path.join(myAnalysis.path_to_dfs, myAnalysis.data_names_df + ".csv")

        reader = pd.read_csv(name_df, sep=None, iterator=True, engine='python')
        inferred_sep = reader._engine.data.dialect.delimiter
        df = pd.read_csv(name_df, delim_whitespace=True)
        if inferred_sep == ",":
            df = pd.read_csv(name_df)

        if query:
            df = df.query(query, engine='python')

        if type_processes != "data":
            print("  -> processing", process["name"], " =", name_df)
        else:
            print("  -> processing data =", name_df)

        # getting the right process
        if type_processes != "data":
            df = df[df["process"] == process["name"]].copy()
        else:
            df = df[df["process"] == myAnalysis.data_names_df].copy()

        if "data" not in list_processes:
            process_name = process["name"]
        else:
            process_name = "data"

        # droping process column as we are now restricted to one process
        df = df.drop(["process"], axis=1)

        # summing content and quadratically summing errors
        df["error"] = df["error"] * df["error"]
        df = df.groupby(['region', 'category', 'systematic', 'variable_low', 'variable_high'])[
            "content", "error"].sum().reset_index(drop=False)
        df["error"] = df["error"]**0.5

        # removing groupby columns
        cols = [c for c in df.columns if "level_" not in c]
        df = df[cols]

        # group to match dictionary structure
        groupByList = [
            "region",
            "category",
            "systematic"]

        grouped = df.groupby(groupByList)

        # looping on groups
        for name, group in grouped:

            category = name[groupByList.index("category")]
            region = name[groupByList.index("region")]
            systematic = name[groupByList.index("systematic")]

            # getting number arrays
            n_array = group["content"].values
            e_array = group["error"].values

            binning = categories[category][region][1]
            binning_tmp = [float(i) for i in binning if i != "inf"]
            binning_tmp2 = [max(binning_tmp) +
                            300. for i in binning if str(i) == "inf"]
            binning = sorted(list(set(binning_tmp + binning_tmp2)))

            if process_name not in histDictionary[category][region].keys():
                continue

            if ("Formula" not in systematic):
                histDictionary[category][region][process_name][systematic] = createHisto(
                    process_name + "_" + category + "_" + region + "_" + systematic, binning)
            if "Formula" not in systematic:
                rootw.array2hist(
                    n_array, histDictionary[category][region][process_name][systematic], e_array)
    return histDictionary


def writeHistos(myAnalysis, histograms, this_type, category):

    for process in histograms:
        for systematic in histograms[process]:
            histo = histograms[process][systematic]
            if process != "data":
                histo.Scale(myAnalysis.luminosity)
            if histo.Integral() < 1E-12:
                if histo.Integral() < 0.:
                    print("WARNING: NEGATIVE INTEGRAL FOR ", histo.GetName(), " = ", histo.Integral())
                    print("SETTING TO 0")
                if this_type != "data":
                    for ib in range(1, histo.GetNbinsX() + 1):
                        histo.SetBinContent(ib, 1E-12)
                        histo.SetBinError(ib, (1E-12)**2.)
                    histo.Scale(1E-12 / histo.Integral())
            if this_type == "data":
                histo.Write(myAnalysis.data_names_dc)
            elif this_type == "signals":
                if systematic == "nominal":
                    histo.Write(process)
                else:
                    histo.Write(
                        process + "_" + systematic.replace("_Up", "Up").replace("_Down", "Down"))
            else:
                if systematic == "nominal":
                    if "Qcd" not in process and myAnalysis.include_category_name:
                        histo.Write(process + category)
                    else:
                        histo.Write(process)
                    # histo.Write(process)
                else:
                    if "Qcd" not in process and myAnalysis.include_category_name:
                        histo.Write(
                            process + category + "_" + systematic.replace("_Up", "Up").replace("_Down", "Down"))
                    else:
                        histo.Write(
                            process + "_" + systematic.replace("_Up", "Up").replace("_Down", "Down"))
    return


def writeRootDatacards(myAnalysis, sigHistDictionary, bkgHistDictionary, dataHistDictionary, userOutdir=None):

    
    outDir = "out_" + myAnalysis.analysis_name + "_" + \
        myAnalysis.dataset + "_" + myAnalysis.version + "/"
    if userOutdir:
        outDir = os.path.join(userOutdir, outDir)
    if os.path.isdir(outDir):
        if len(glob.glob(outDir + "*.root")) != 0:
            for file in glob.glob(outDir + "*.root"):
                os.remove(file)
    else:
        os.makedirs(outDir)

    for category in dataHistDictionary:
        outRoot = outDir + category.replace("::", "_") + "_shapes.root"
        f = rootw.Open(outRoot, 'RECREATE')

        for region in dataHistDictionary[category]:

            if "nominal" in dataHistDictionary[category][region]["data"]:
                if len(dataHistDictionary[category][region]["data"]["nominal"]) == 0:
                    continue
            else:
                continue

            savdir = rootw.GetDirectory(rootw.GetPath())
            tmp_region = region
            if tmp_region != "SR":
                tmp_region += category
            if not f.GetDirectory(tmp_region):
                savdir.mkdir(tmp_region)
            f.cd(tmp_region)

            writeHistos(
                myAnalysis, dataHistDictionary[category][region], "data", category)
            writeHistos(
                myAnalysis, sigHistDictionary[category][region], "signals", category)
            writeHistos(
                myAnalysis, bkgHistDictionary[category][region], "backgrounds", category)

            adir_1 = f.GetDirectory(tmp_region)
            adir_1.cd("..")
    return


def writeTxtDatacards(myAnalysis, sigHistDictionary, bkgHistDictionary, dataHistDictionary, userOutdir=None):

    outDir = "out_" + myAnalysis.analysis_name + "_" + \
        myAnalysis.dataset + "_" + myAnalysis.version + "/"

    if userOutdir:
       outDir = os.path.join(userOutdir, outDir)

    if len(glob.glob(outDir + "*.txt")) != 0:
        for file in glob.glob(outDir + "*.txt"):
            os.remove(file)

    name_df = os.path.join(myAnalysis.path_to_dfs, myAnalysis.data_names_df + ".csv")
    reader = pd.read_csv(name_df, sep=None, iterator=True, engine='python')
    inferred_sep = reader._engine.data.dialect.delimiter
    local_df = pd.read_csv(name_df, delim_whitespace=True)
    if inferred_sep == ",":
        local_df = pd.read_csv(name_df)

    for category in dataHistDictionary:
        outTxt = outDir + category.replace("::", "_") + "_higgsDataCard.txt"
        outRoot = category.replace("::", "_") + "_shapes.root"

        local_df = local_df[local_df["category"] == category].copy()
        groups = local_df.groupby(["variable"])
        local_variable = ""
        for name, group in groups:
            local_variable = name
            break

        with open(outTxt, 'w') as text_file:

            write_dc_header(text_file, outRoot)
            write_dc_data_section(text_file, category, dataHistDictionary)
            write_dc_processes_section(
                text_file, category, dataHistDictionary, sigHistDictionary, bkgHistDictionary, myAnalysis)
            write_dc_rate_section(
                text_file, category, dataHistDictionary, sigHistDictionary, bkgHistDictionary)
            write_dc_systematics_section(text_file, myAnalysis, local_variable, category,
                                         dataHistDictionary, sigHistDictionary, bkgHistDictionary)

            text_file.write("\n")

            regions_for_this_category = list(dataHistDictionary[category].keys())

            if myAnalysis.appendix:
                for ele in myAnalysis.appendix:
                    if "adhoc::" in ele:
                        toPrint_crossDatacards = ele.replace("{category}", category).replace("adhoc::","").replace("::", "_")
                        text_file.write(toPrint_crossDatacards + "\n")
                        continue
                    if (ele.split("{category}")[0] not in regions_for_this_category) and ("SR" not in ele): continue
                    found = False
                    if ("SR" in ele):
                        regions_for_this_category2 = list(dataHistDictionary[category].keys())
                        for reg in regions_for_this_category2:
                            if "SR" in reg: continue
                            if reg in ele:
                                found = True
                        if not found: continue
                    toPrint = ele.replace("{category}", category).replace("::", "_")
                    text_file.write(toPrint + "\n")

def write_dc_header(text_file, outRoot):

    text_file.write("imax    *     number of categories\n")
    text_file.write("jmax    *     number of samples minus one\n")
    text_file.write("kmax    *     number of nuisance parameters\n")
    text_file.write(
        "-------------------------------------------------------------------------------\n")
    text_file.write("shapes * * " + outRoot +
                    " $CHANNEL/$PROCESS $CHANNEL/$PROCESS_$SYSTEMATIC")
    text_file.write("\n")
    text_file.write("\n")

    return


def write_dc_data_section(text_file, category, dataHistDictionary):

    t = Template("{% for n in list %}{{n}}\t" "{% endfor %}")

    text_file.write("bin\t ")

    this_list = []
    for region in dataHistDictionary[category]:
        if "nominal" in dataHistDictionary[category][region]["data"]:
            if len(dataHistDictionary[category][region]["data"]["nominal"]) != 0:
                tmp_region = region
                if tmp_region != "SR":
                    tmp_region += category
                this_list += [tmp_region]
    text_file.write(t.render(list=this_list))

    text_file.write("\n")
    text_file.write("observation\t")

    # if(dataHistDictionary[category]["SR"]["data"]["nominal"].Integral()>1E-12):
    #    dataHistDictionary[category]["SR"]["data"]["nominal"].Scale(1./abs(dataHistDictionary[category]["SR"]["data"]["nominal"].Integral()))

    text_file.write(t.render(list=[str(int(dataHistDictionary[category][region]["data"]["nominal"].Integral()))
                                   for region in dataHistDictionary[category]
                                   if "nominal" in dataHistDictionary[category][region]["data"]]))

    text_file.write("\n")
    text_file.write(
        "-------------------------------------------------------------------------------\n")

    return


def write_dc_processes_section(text_file, category, dataHistDictionary,
                               sigHistDictionary, bkgHistDictionary, myAnalysis):

    t = Template("{% for n in list %}{{n}}\t" "{% endfor %}")

    text_file.write("bin\t")

    this_list = []

    for region in dataHistDictionary[category]:
        list_keys = list(sigHistDictionary[category][region].keys())
        list_keys += list(bkgHistDictionary[category][region].keys())

        for process in list_keys:
            if "nominal" in dataHistDictionary[category][region]["data"]:
                if len(dataHistDictionary[category][region]["data"]["nominal"]) != 0:
                    tmp_region = region
                    if tmp_region != "SR":
                        tmp_region += category
                    this_list.append(tmp_region)

    text_file.write(t.render(list=this_list))

    text_file.write("\n")
    text_file.write("\n")
    text_file.write("process\t ")

    this_list = []

    for region in dataHistDictionary[category]:
        list_keys = list(sigHistDictionary[category][region].keys())
        backgrounds_list = list(bkgHistDictionary[category][region].keys())
        for j, s in enumerate(backgrounds_list):
            if s != "Qcd" and myAnalysis.include_category_name:
                backgrounds_list[j] = s + category
        list_keys += backgrounds_list

        for process in list_keys:
            if "nominal" in dataHistDictionary[category][region]["data"]:
                if len(dataHistDictionary[category][region]["data"]["nominal"]) != 0:
                    this_list.append(process)

    text_file.write(t.render(list=this_list))

    text_file.write("\n")
    text_file.write("process\t ")

    this_list = []

    for region in dataHistDictionary[category]:
        if "nominal" in dataHistDictionary[category][region]["data"]:
            if len(dataHistDictionary[category][region]["data"]["nominal"]) != 0:
                this_list += [str(number)
                              for number in range(-len(sigHistDictionary[category][region]) + 1, 1)]
                this_list += [str(number) for number in range(1,
                                                              1 + len(bkgHistDictionary[category][region]))]

    text_file.write(t.render(list=this_list))

    text_file.write("\n")


def write_dc_rate_section(text_file, category, dataHistDictionary, sigHistDictionary, bkgHistDictionary):

    t = Template("{% for n in list %}{{n}}\t" "{% endfor %}")

    text_file.write("rate\t ")

    this_list = []

    for region in dataHistDictionary[category]:
        for process in sigHistDictionary[category][region]:
            if dataHistDictionary[category][region]["data"]:
                value = sigHistDictionary[category][region][process]["nominal"].Integral(
                )
                if value < 1E-12:
                    value = 1E-12
                this_list.append(str(value))
        for process in bkgHistDictionary[category][region]:
            if dataHistDictionary[category][region]["data"]:
                value = bkgHistDictionary[category][region][process]["nominal"].Integral(
                )
                if value < 1E-12:
                    value = 1E-12
                this_list.append(str(value))

    text_file.write(t.render(list=this_list))

    text_file.write("\n")
    text_file.write(
        "-------------------------------------------------------------------------------\n")


def write_dc_systematics_section(text_file, myAnalysis, local_variable,
                                 category, dataHistDictionary, sigHistDictionary, bkgHistDictionary):

    nameSysts = {}

    for systematic in myAnalysis.systematics:
        if (systematic["type"] != "lnN") and (systematic["type"] != "lnU"):
            continue
        if systematic["name"] + "::" + systematic["type"] not in nameSysts:
            nameSysts[systematic["name"] + "::" + systematic["type"]] = {}
        thisDict = {}
        for process in systematic["apply_to"]:
            value = "-"
            condition = True
            if ("variable==" + local_variable not in str(systematic["when"])):
                condition = False
            if str(systematic["when"]) == "True":
                condition = True

            value = str(systematic["value"])

            if "signals" in process:
                for process2 in myAnalysis.signals:
                    if condition:
                        thisDict[process2["name"]] = value
            elif "backgrounds" in process:
                for process2 in myAnalysis.backgrounds:
                    if condition:
                        thisDict[process2["name"]] = value
            else:
                if condition:
                    thisDict[process] = value

        if systematic["name"] + "::" + systematic["type"] not in nameSysts:
            nameSysts[systematic["name"] + "::" +
                      systematic["type"]] = thisDict
        else:
            nameSysts[systematic["name"] + "::" + systematic["type"]] = dict(
                list(nameSysts[systematic["name"] + "::" + systematic["type"]].items()) + list(thisDict.items()))

    nameShapeSysts = []
    shapeSystsValues = {}

    nameNormSysts = nameSysts.keys()
    normSystsValues = {}

    for systematic2 in nameNormSysts:
        normSystsValues[systematic2] = {}
        for region in dataHistDictionary[category]:
            normSystsValues[systematic2][region] = []
            for process in sigHistDictionary[category][region]:
                if (systematic2 in nameSysts) and (process in nameSysts[systematic2]):
                    normSystsValues[systematic2][region].append(nameSysts[systematic2][process])
                else:
                    normSystsValues[systematic2][region].append("-")
            for process in bkgHistDictionary[category][region]:
                if (systematic2 in nameSysts) and (process in nameSysts[systematic2]):
                    normSystsValues[systematic2][region].append(nameSysts[systematic2][process])
                else:
                    normSystsValues[systematic2][region].append("-")

    for region in dataHistDictionary[category]:
        for process in sigHistDictionary[category][region]:
            for systematic2 in sigHistDictionary[category][region][process]:
                if systematic2 not in nameShapeSysts:
                    nameShapeSysts.append(systematic2)
        for process in bkgHistDictionary[category][region]:
            for systematic2 in bkgHistDictionary[category][region][process]:
                if systematic2 not in nameShapeSysts:
                    nameShapeSysts.append(systematic2)

    for nameShapeSyst in nameShapeSysts:
        shapeSystsValues[nameShapeSyst] = {}
        for region in dataHistDictionary[category]:
            shapeSystsValues[nameShapeSyst][region] = []
            for process in sigHistDictionary[category][region]:
                if nameShapeSyst in sigHistDictionary[category][region][process].keys():
                    shapeSystsValues[nameShapeSyst][region].append("1")
                else:
                    shapeSystsValues[nameShapeSyst][region].append("-")
            for process in bkgHistDictionary[category][region]:
                if nameShapeSyst in bkgHistDictionary[category][region][process].keys():
                    shapeSystsValues[nameShapeSyst][region].append("1")
                else:
                    shapeSystsValues[nameShapeSyst][region].append("-")

    for syst in normSystsValues:
        text_file.write(syst.replace("::lnN", "").replace("::lnU", "") + " \t ")
        text_file.write(syst.split("::")[1] + " \t ")
        for region in normSystsValues[syst]:
            for value in normSystsValues[syst][region]:
                text_file.write(value + " \t ")
        text_file.write("\n")

    for syst in shapeSystsValues:
        if "_Down" in syst:
            continue
        if "nominal" in syst:
            continue
        syst_write = syst.replace("_Up", "")
        text_file.write(syst_write + " \t ")
        text_file.write("shape \t ")
        for region in shapeSystsValues[syst]:
            for value in shapeSystsValues[syst][region]:
                text_file.write(value + " \t ")
        text_file.write("\n")

    text_file.write("\n")

    for region in dataHistDictionary[category]:
        if "nominal" in dataHistDictionary[category][region]["data"]:
            if len(dataHistDictionary[category][region]["data"]["nominal"]) != 0:
                tmp_region = region
                if tmp_region != "SR":
                    tmp_region += category
                text_file.write(tmp_region + " autoMCStats "+str(myAnalysis.autoMCStats_threshold)+"\n")


def main(config_file, outdir=None, in_dir=None, query=None):

    # read config
    myAnalysis = Analysis(config_file)
    categories = getCategorization(myAnalysis, query)
    if in_dir:
        myAnalysis.path_to_dfs = in_dir

    # create histograms
    sigHistDictionary = createDictionary(myAnalysis, categories, "signals", query)
    bkgHistDictionary = createDictionary(myAnalysis, categories, "backgrounds", query)
    dataHistDictionary = createDictionary(myAnalysis, categories, "data", query)

    # fill histograms
    parallel = alphatwirl.parallel.build_parallel(
        parallel_mode="multiprocessing",
        processes=4,
    )
    parallel.begin()

    myHists = [sigHistDictionary, bkgHistDictionary, dataHistDictionary]
    myTypes = ["signals", "backgrounds", "data"]
    for i in range(0, len(myHists)):
        parallel.communicationChannel.put(
            fillDictionary, myAnalysis, myHists[i], myTypes[i], categories, query=query)

    finalDicts = parallel.communicationChannel.receive()
    parallel.end()

    # write histograms & text files
    writeRootDatacards(myAnalysis, finalDicts[0], finalDicts[1], finalDicts[2], userOutdir=outdir)
    writeTxtDatacards(myAnalysis, finalDicts[0], finalDicts[1], finalDicts[2], userOutdir=outdir)

    # profiling
    # alphatwirl.misc.print_profile_func(
    #     func=functools.partial(fillDictionary,myAnalysis, sigHistDictionary, "signals"), profile_out_path=None
    # )
